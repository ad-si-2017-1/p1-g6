# Servidor IRC

Servidor irc com programação socket.

# Pré-requisito

[Node.js](https://nodejs.org/en/)

> [Instalando via package manager](https://nodejs.org/en/download/package-manager)

# Como usar

Toda a documentação com comandos disponiveis no servidor e como usa-los esta disponivel no [Manual do Usuário](https://gitlab.com/ad-si-2017-1/p1-g6/wikis/home) dentro da wiki do repositorio.
    
```    
    https://gitlab.com/ad-si-2017-1/p1-g6/wikis/home
    
```        

# Membros

- Rodrigo Rodrigues Filho
- Matheus Miranda Cavalcante
- Vinicius Candido de Carvalho
- Victor Ferreira
- Lucas Zenha
- Pedro Andrade
- Gisana Cristina Alves Bueno
