irc = require('./config/server');
// Keep track of the chat clients
var clients = [];
// Start a TCP Server
net.createServer(function (socket) {
  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 
  socket.channel = "#Lobby";
  socket.countKick = 0;

  
  // Put this new client in the list
  clients.push(socket);
 
  // Send a nice welcome message and announce
  console.log("Welcome " + socket.name + "\n");
  irc.broadcast(socket.name + " se conectou no chat " + socket.channel+"\n", clients, socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    var mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var args = mensagem.split(" "); 
    if      ( args[0] == "NICK") irc.nick(args, clients, socket); // se o primeiro argumento for NICK
    else if ( args[0] == "USER") irc.user(args, mensagem, clients, socket);
    else if ( args[0] == "JOIN") irc.join(args, mensagem, clients, socket);
    else if ( args[0] == "KICK") irc.kick(args, clients, socket);
    else if ( args[0] == "PART") irc.part(args, mensagem, clients, socket);
    else if ( args[0] == "QUIT") irc.quit(args, mensagem, clients, socket); 
    else if ( args[0] == "PRIVMSG") irc.privmsg(args, mensagem, clients, socket);
    else if ( args[0] == "LIST") irc.list(args, clients, socket);

    else  irc.broadcast(socket.name + " "+ socket.channel +"> " + data,clients, socket);
  });
  // Remove the client from the list when it leaves
  socket.on('end', function () {
  
    clients.splice(clients.indexOf(socket), 1);
    irc.broadcast("O usuário "+ socket.name +" saiu do IRC.\n", clients, socket);
     
  });

}).listen(6667);
// Put a friendly message on the terminal of the server.
  
console.log("Chat server running at port 6667\n");