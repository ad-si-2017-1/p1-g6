Comando: LIST
O comando é usado para listar os usuários conectados em um determinado canal.

Sintaxe: LIST [<canal> *( "," <canal> )]

Caso seja inserido o canal, deve ser exibido apenas os usuários daquele canal.

Ex.:
LIST    			>>> irá exibir todos os usuarios de todos os canais disponíveis.
LIST #canalExemplo  >>> exibe os usuários do canal #canalExemplo