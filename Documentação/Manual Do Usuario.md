
Para iniciar o servidor deve-se utilizar o comando nodejs irc-chat.js no diretório do projeto, que irá rodar na porta 6667.


O cliente conecta com o servidor por meio do telnet: "telnet <IP/DNS do servidor> 6667".


# Comandos do Chat IRC:
```  
Comando NICK: "NICK [NickDesejado]"
Comando JOIN: "JOIN [#CanalDesejado] / JOIN 0" '#' pode ser substituído por '&', '+' ou '!'; 0 retorna ao #Lobby
Comando LIST: "LIST"
```  

# Manual do Usuário do IRC-Chat

- UTILIZAÇÃO

O IRC-Chat utiliza protocolo Telnet, presente Bash do Linux. 

O Bash pode ser acessado pelo próprio Ubuntu ou pelo módulo Bash for Windows que pode ser instalado se a máquina possuir o Windows 10

>[Como instalar o Bash Ubuntu no Windows 10] (https://msdn.microsoft.com/pt-br/commandline/wsl/install_guide)

- LISTA DE COMANDOS

O IRC-Chat possui comandos que podem ser utilizados para alterar configurações de usuários e/ou usar funções do chat para comunicação.

Segue abaixo a lista links dos comandos:

>[Comando JOIN] (ComandoJOIN.txt)

>[Comando KICK] (ComandoKICK.txt)

>[Comando NICK] (ComandoNICK.txt)

>[Comando PRIVMSG] (ComandoPRIVMSG.txt)

>[Comando QUIT] (ComandoQUIT.txt)

>[Comando USER] (ComandoUSER.txt)

- CANAIS DO CHAT

"#LOBBY" é o canal pardrão do IRC-Chat que direciona todos os usuários conectados.
Estes canais possuem regras e nomeclaturas, conforme especificado na [Documentação de Canais] (DocumentacaoCanais.txt)

