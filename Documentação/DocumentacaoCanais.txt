Os nomes dos canais s�o strings (come�ando com os caracteres'&', '#', '+' ou '!') de comprimento at� 
cinq�enta (50) caracteres. Apesar da exig�ncia de que o primeiro caractere seja '&', '#', '+' ou '!',
a �nica restri��o a um nome de canal � que n�o contenha quaisquer espa�os (' '), control G (^G ou ASCII 7), 
ou uma v�rgula (','). 
O espa�o � usado como separador de par�metros e o comando � usado pelo protocolo como separador de um item da lista). 
Os dois pontos (':') tamb�m podem ser usados como um delimitador para a m�scara de canal. 
Os nomes de canais n�o diferenciam mai�sculas de min�sculas.