3.1.3 User message 

Comando: USER
Parâmetros: <nome de usuário> <modo> <não utilizado> <nome real>

O comando USER é utilizado no início da conexão para especificar o nome de usuário, endereço do host (IP) e nome real de um novo usuário.

O parâmetro <modo> deverá ser numérico e pode ser utilizado para setar automaticamente modos do usuário ao se registrar no servidor.


O <nome real> pode conter espaços.

Respostas numéricas (ver seção 5):

ERR_NEEDMOREPARAMS 
ERR_ALREADYREGISTRED

Exemplo:

USER guest 0 * :Ronnie Reagan 
Usuário se registrando com nome de usuário "guest" e nome real "Ronnie Reagan".

USER guest 8 * :Ronnie Reagan
Usuário se registrando com nome de usuário "guest" e nome real "Ronnie Reagan" e setando modo invisível (por causa do número 8.)