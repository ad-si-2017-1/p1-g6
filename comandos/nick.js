nick = function (args, clients, socket) {
  if(!args[1]){
    socket.write("Nome invalido!\n");
  }else {
    var nickUsed = false;
    clients.forEach(function (client){
      if(String(args[1]).trim() === client.name){
        nickUsed = true;
        socket.write("Nome esta em uso \n");
        return;
      }
    });
    if(!nickUsed) {
      var tempName = socket.name;
      socket.name = String(args[1]).trim();
      socket.write("Nome alterado com sucesso\n");
      broadcast(tempName + " Alterou o nick para " + socket.name + "\n",clients, socket);
    
    }
  }
}

module.exports = nick;