  user = function(args, mensagem, clients, socket){
    mensagem2 = mensagem.split(":"); //divide o conteúdo da mensagem em duas partes, uma antes e outra depois dos : (DOIS PONTOS)    
    var realname = mensagem2[1]; // Pega apenas o conteúdo depois dos :
    var user     = String(args[1]);
    var mode     = String(args[2]);

  if (args.length < 6){
      socket.write("USER :Not enough parameters \n");
      return;
    } 
    else{
        if(mode.charAt(0) != '0' & mode.charAt(0) != '8'){
          socket.write("Modo inexistente, modos válidos: \n 0 - visível \n 8 - invisível \n");
          return;   
        }
        else{
          var nickUsed = false;
          clients.forEach(function (client){
            if(String(args[1]).trim() === client.name){
              nickUsed = true;
              socket.write("Unauthorized command (already registered) \n");
              return;
            }
          });
          if(!nickUsed) {
            socket.name = user;
            socket.mode     = mode;
            socket.realname = realname;
            socket.write("Welcome to the Internet Relay Network " + user + "\n"); 
          }
      
      }
    }
  }
  module.exports = user;
