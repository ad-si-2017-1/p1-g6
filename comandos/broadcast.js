
broadcast = function(message, clients, sender) {
	clients.forEach(function (client) {
	  // Don't want to send it to sender
	  if (client === sender) return;
	  
	  if(sender.channel == client.channel){
	  	client.write(message);
	  }
	});
	// Log it to the server output too
	
	process.stdout.write(message)
	
}

module.exports = broadcast;