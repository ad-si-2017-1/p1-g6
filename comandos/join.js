Channel = require('../comandos/Channels.js');
join =   function (args, mensagem, clients, socket) {
    // Array para armazenar os chats
    //var channel = [];
    this.mensagem = mensagem.split(args[1]);
    if(!socket.realname){
        socket.write("Voce deve se registrar primeiro, antes de poder entrar nos canais\nExemplo: USER guest 0 * : Seu Nome\n");
        return;
    }

    if ( ! args[1]) { // Se o chat não for digitado                                         
        socket.write("Erro: Nome INVÁLIDO, Digite o chat que deseja se conectar\n"); 
    }
    else if(args[1].charAt(0)!='&' & args[1].charAt(0)!='#' & args[1].charAt(0)!='+' & args[1].charAt(0)!='!'
    & args[1].charAt(0)!='0'){
        socket.write("Para entrar em um chat utilize os prefixos '&', '#', '+' ou '!'\n");
    }
    else if (args[1].length>50){
        socket.write("Erro: nome muito grande, escolha um nome menor.\n");
    }
    else if (args[1] == "0") {
	if(socket.mode === '0') irc.broadcast("O usuário "+ socket.name +" voltou para o #Lobby\n", clients, socket);
    socket.channel = "#Lobby";    
	socket.write("Voce saiu do chat e voltou para o Lobby!\n");
        }   
    else if (this.mensagem[1]){
    socket.write("Erro: Nome INVÁLIDO, Digite o chat que deseja se conectar, sem espaços...\n");                                             
        }     
    else {

        if(socket.channel === args[1]){
            socket.write("Voce ja esta neste canal \n");
            return;
        }

    
        irc.broadcast("O usuario "+ socket.name +" se conectou no chat "+ args[1]+"\n", clients, socket);
        socket.countKick = 0;
        socket.channel = args[1];

        socket.write("Bem vindo! "+ socket.name +" ao chat "+ args[1]+"\n", clients, socket);

        // Transmite a mensagem a todos nos canais se tiver no modo visivel:
        if(socket.mode === '0') irc.broadcast("O usuário "+ socket.name +" se conectou no chat "+ args[1]+"\n", clients, socket);
        return;
	}
  
}
module.exports = join;