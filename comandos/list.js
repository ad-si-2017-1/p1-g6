/**
 * Deve ser verificado ainda: busca em multiplos canais e direcionamento ao servidor.
 */
list = function(args, clients, socket){
    if(!args[1]){
        clients.forEach(function(client) {
            socket.write("Canal: "+ client.channel +" -- Usuario: "+ client.name + "\n");
    });
    }//Valida se o canal foi digitado corretamente
    else if(args[1].charAt(0)!='&' & args[1].charAt(0)!='#' & args[1].charAt(0)!='+' & args[1].charAt(0)!='!' & args[1].charAt(0)!='0'){
        socket.write("Para listar os usuarios deve-se inserir o canal (chat) iniciado por: '&', '#', '+' ou '!'\n");
        return;
    }
    else{
        clients.forEach(function(client) {
            for (var i = 1; i < args.length; i++) {
                if(args[1] == client.channel){
                    socket.write("Canal: "+ client.channel +" -- Usuario: "+ client.name + "\n");
                }
            }
        });
    }
}
module.exports = list;