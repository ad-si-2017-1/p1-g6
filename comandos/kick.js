kick = function(args, clients, socket){
	var nick = false;
	var totalChannel = 0;
	if(!args[1]){
		socket.write("Nome invalido");
		return;
	}
	if(socket.channel === "#Lobby"){
		socket.write("Voce nao pode expulsar ninguem no #Lobby\n");
		return;
	}

	clients.forEach(function (client){
		if(socket.channel === client.channel) totalChannel++;
	});

	clients.forEach(function (client){

		if (client === socket) return;
		if(socket.channel === client.channel){
			if(args[1] === client.name){
				socket.write("Voce votou para o banimento de "+ client.name + "\n");
				nick = true;
				client.countKick++;

				if(client.countKick > (totalChannel/2)|0){
					irc.broadcast(client.name + " foi expulso da sala \n", clients, socket);
					client.channel = "#Lobby";
				}
			}
		}
	});

	if(!nick){
		socket.write("Nick nao encontrado\n");
	}


}

module.exports = kick;